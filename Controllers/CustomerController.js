const Customer = require("../Models/CustomerModel");
module.exports = {
	CreateCustomer: function (request, resultat) {
		const newCustomer = {
			name: request.body.name,
			email: request.body.email,
			password: request.body.password,
			phone: request.body.phone,
			address: request.body.address,
			city: request.body.city,
		};
		Customer.create(newCustomer, (error, customer) => {
			if (error) {
				resultat.status(500).json({
					message: error,
					statut: 500,
				});
			} else {
				resultat.status(200).json({
					message: "customer added",
					statut: 200,
					data: customer,
				});
			}
		});
	},
	GetALLCustomer: function (req, res) {
		Customer.find({})
			.populate("id_order")
			.exec((err, listcustomers) => {
				if (err)
					res.status(500).json({
						message: err,
						statut: 500,
					});
				else
					res.status(200).json({
						message: "Customer founded",
						statut: 200,
						data: listcustomers,
					});
			});
	},
	pushOrder: function (req, res) {
		Customer.updateOne(
			{ _id: req.params.id },
			{ $push: { id_order: req.body.id_order } },
			function (err, order) {
				if (err) {
					res.json({ message: "error", status: 500, data: null });
				} else {
					res.json({ message: "order is pushed", status: 200, data: order });
				}
			}
		);
	},
	pullOrder: function (req, res) {
		Customer.updateOne(
			{ _id: req.params.id },
			{ $pull: { id_order: req.body.id_order } },
			function (err, order) {
				if (err) {
					res.json({ message: "error", status: 500, data: null });
				} else {
					res.json({
						message: "order is removed from order",
						status: 200,
						data: order,
					});
				}
			}
		);
	},
};
