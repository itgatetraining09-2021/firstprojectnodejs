const Order = require("../Models/OrderModel");
module.exports = {
	CreateOrder: function (request, resultat) {
		const newOrder = {
			date: request.body.date,
			price: request.body.price,
		};
		Order.create(newOrder, (error, order) => {
			if (error) {
				resultat.status(500).json({
					message: error,
					statut: 500,
				});
			} else {
				resultat.status(200).json({
					message: "order added",
					statut: 200,
					data: order,
				});
			}
		});
	},
	GetALLOrder: function (req, res) {
		Order.find({})
			.populate("id_product")
			.exec((err, listorders) => {
				if (err)
					res.status(500).json({
						message: err,
						statut: 500,
					});
				else
					res.status(200).json({
						message: "Order founded",
						statut: 200,
						data: listorders,
					});
			});
	},
	pushProduct: function (req, res) {
		Order.updateOne(
			{ _id: req.params.id },
			{ $push: { id_product: req.body.id_product } },
			function (err, product) {
				if (err) {
					res.json({ message: "error", status: 500, data: null });
				} else {
					res.json({ message: "product is pushed", status: 200, data: product });
				}
			}
		);
	},
	/* pullOrder: function (req, res) {
		Customer.updateOne(
			{ _id: req.params.id },
			{ $pull: { id_order: req.body.id_order } },
			function (err, order) {
				if (err) {
					res.json({ message: "error", status: 500, data: null });
				} else {
					res.json({
						message: "order is removed from order",
						status: 200,
						data: order,
					});
				}
			}
		);
	}, */
};
