const Product = require("../Models/ProductModel");
module.exports = {
	CreateProduct: function (request, resultat) {
		const newProduct = {
			name: request.body.name,
			price: request.body.price,
			description: request.body.description,
			// image: request.body.image,
			id_category: request.body.id_category, //2eme etape relation one
			id_provider: request.body.id_provider, //2eme etape relation one
		};
		Product.create(newProduct, (error, product) => {
			if (error) {
				resultat.status(500).json({
					message: error,
					statut: 500,
				});
			} else {
				resultat.status(200).json({
					message: "product added",
					statut: 200,
					data: product,
				});
			}
		});
	},
	GetALLProduct: function (req, res) {
		Product.find({})
			.populate("id_category")
			.populate("id_provider")
			.exec((err, listproducts) => {
				if (err)
					res.status(500).json({
						message: err,
						statut: 500,
					});
				else
					res.status(200).json({
						message: "Product founded",
						statut: 200,
						data: listproducts,
					});
			});
	},
};
