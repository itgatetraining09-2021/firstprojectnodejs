const User = require("../Models/UserModel");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const randtoken = require("rand-token");

var RefreshTokens = [];

const nodemailer = require("nodemailer");

module.exports = {
	CreateUser: function (req, res) {
		const newuser = {
			name: req.body.name,
			email: req.body.email,
			password: req.body.password,
			phone: req.body.phone,
			image: req.file.filename,
		};
		User.create(newuser, (err, user) => {
			if (err)
				res.status(500).json({
					message: err,
					statut: 500,
				});
			else
				res.status(200).json({
					message: "user added",
					statut: 200,
					data: user,
				});
		});
	},
	GetALLUser: function (req, res) {
		var size = 4; //size howa nbre user par page me3neha kol page feha 4
		var page = Math.max(0, parseInt(req.params.page));
		User.find({})
			.select("name phone")
			.sort({ name: "1" })
			.skip(size * page)
			.limit(size)
			.exec((err, listusers) => {
				//1 ou bien -1 décroissant
				if (err)
					res.status(500).json({
						message: err,
						statut: 500,
					});
				else
					res.status(200).json({
						message: "user founded",
						statut: 200,
						data: listusers,
					});
			});
	},	
	GetALLUser1: function (req, res) {
		User.find({}).exec((err, listusers) => {
			//1 ou bien -1 décroissant
			if (err)
				res.status(500).json({
					message: err,
					statut: 500,
				});
			else
				res.status(200).json({
					message: "user founded",
					statut: 200,
					data: listusers,
				});
		});
	},
	GetUserById: function (req, res) {
		User.findById({ _id: req.params.id }).exec((err, user) => {
			if (err)
				res.status(500).json({
					message: err,
					statut: 500,
				});
			else
				res.status(200).json({
					message: "user found by id",
					statut: 200,
					data: user,
				});
		});
	},
	DeleteUser: function (req, res) {
		User.deleteOne({ _id: req.params.id }).exec((err, user) => {
			if (err)
				res.status(500).json({
					message: err,
					statut: 500,
				});
			else
				res.status(200).json({
					message: "user deleted",
					statut: 200,
					data: user,
				});
		});
	},
	UserUpdate: function (req, res) {
		User.updateOne({ _id: req.params.id }, req.body).exec(
			(err, userupdated) => {
				if (err)
					res.status(500).json({
						message: err,
						statut: 500,
					});
				else
					res.status(200).json({
						message: "user update",
						statut: 200,
						data: userupdated,
					});
			}
		);
	},
	AuthenticateUser: function (req, res, next) {
		User.findOne({ email: req.body.email }, function (err, userInfo) {
			if (err || !userInfo) {
				res.json({
					status: "error",
					message: "Invalid email/password!!!",
					data: err,
				});
			} else {
				if (bcrypt.compareSync(req.body.password, userInfo.password)) {
					const token = jwt.sign(
						{
							//sign pour creation de token
							id: userInfo._id,
							user: userInfo,
						},
						req.app.get("secretKey"),
						{ expiresIn: "2h" }
					);
					var refreshToken = randtoken.uid(256);
					RefreshTokens[refreshToken] = userInfo._id;
					res.json({
						status: "success",
						message: "user found!!!",
						data: {
							user: userInfo,
							token: token,
							refreshtoken: refreshToken,
						},
					});
				} else {
					res.json({
						status: "error",
						message: "Invalid email/password!!!",
						data: null,
					});
				}
			}
		});
	},
	RefreshToken: function (req, res) {
		var id = req.body._id;
		var RefreshToken = req.body.refreshToken;
		console.log("refresh", RefreshToken in RefreshTokens);
		if (RefreshToken in RefreshTokens && RefreshTokens[RefreshToken] == id) {
			var user = { id: id };
			var token = jwt.sign(user, req.app.get("secretKey"), {
				expiresIn: "2h",
			});
			res.json({
				accesstoken: token,
			});
		} else {
			res.sendStatus(401);
		}
	},
	LogoutUser: function (req, res, next) {
		var refreshToken = req.body.refreshToken;
		console.log("refreshTokens : ", RefreshTokens);
		jwt.verify(
			req.headers["x-access-token"],
			req.app.get("secretKey"),
			(err, decoded) => {
				if (refreshToken in RefreshTokens) {
					delete RefreshTokens[refreshToken];
					res.json({
						status: "success",
						message: "Logout",
						data: refreshToken,
					});
				} else {
					res.json({
						status: "error",
						message: "No Logout",
						data: null,
					});
				}
			}
		);
	},
	SendMailUser: function (req, res) {
		var data = {
			from: "testmaroua92@gmail.com",
			to: req.body.to,
			subject: req.body.subject,
			text: req.body.text,
		};
		var transporter = nodemailer.createTransport({
			service: "gmail",
			auth: {
				user: "testmaroua92@gmail.com",
				pass: "TestMaroua2021",
			},
		});
		transporter.sendMail(data, function (error, info) {
			if (error) {
				console.log(error);
				return res.json({ err: "error in email" + error });
			} else {
				return res.json({
					message: "email has been send",
				});
			}
		});
	},
};
