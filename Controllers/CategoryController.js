const Category = require("../Models/CategoryModel")
module.exports={
    CreateCategory: function(request, resultat){
        const newCategory ={
            title: request.body.title,
            description: request.body.description,
        };
        Category.create(newCategory, (error, category)=>{
            if (error) {
                resultat.status(500).json({
                    message: error,
                    statut:500
                })
            } else {
                resultat.status(200).json({
                    message: "Category added",
                    statut: 200,
                    data: category
                })
            }
        })
    }
}