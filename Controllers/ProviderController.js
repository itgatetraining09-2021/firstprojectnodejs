const Provider = require("../Models/ProviderModel")
module.exports={
    CreateProvider: function(request, resultat){
        const newProvider ={
            name: request.body.name,
			email: request.body.email,
			password: request.body.password,
			phone: request.body.phone,
            matricule: request.body.matricule,
            company: request.body.company,
            service: request.body.service
        };
        Provider.create(newProvider, (error, provider)=>{
            if (error) {
                resultat.status(500).json({
                    message: error,
                    statut:500
                })
            } else {
                resultat.status(200).json({
                    message: "provider added",
                    statut: 200,
                    data: provider
                })
            }
        })
    }
}