const ordercontroller = require("../Controllers/OrderController");
const route = require("express").Router();

route.post("/save", ordercontroller.CreateOrder);
route.get("/getAll", ordercontroller.GetALLOrder);
route.put("/pushproduct/:id", ordercontroller.pushProduct);
//route.put("/pullorder/:id", ordercontroller.pullOrder);
module.exports = route;
