const customercontroller = require("../Controllers/CustomerController");
const route = require("express").Router();

route.post("/save", customercontroller.CreateCustomer);
route.get("/getAll", customercontroller.GetALLCustomer);
route.put("/pushorder/:id", customercontroller.pushOrder);
route.put("/pullorder/:id", customercontroller.pullOrder);

module.exports = route;
