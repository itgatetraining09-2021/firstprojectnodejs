const usercontroller = require("../Controllers/UserController");
const route = require("express").Router();
const auth = require("../middelware/authentification");


const multer = require("../middelware/uploads");

route.post("/save", multer.single("file"), usercontroller.CreateUser);

//route.get('/all/:page',usercontroller.GetALLUser)
route.get("/all", usercontroller.GetALLUser1);
//route.get('/getone/:id',auth.validateUser,usercontroller.GetUserById)
route.get("/getone/:id", usercontroller.GetUserById);
route.put("/update/:id", usercontroller.UserUpdate);
//route.delete('/delete/:id',auth.validateUser,usercontroller.DeleteUser)
route.delete("/delete/:id", usercontroller.DeleteUser);

route.post("/auth", usercontroller.AuthenticateUser);
route.post("/refresh", auth.validateUser, usercontroller.RefreshToken);
route.post("/logout", usercontroller.LogoutUser);

route.post("/sendmail", usercontroller.SendMailUser);

module.exports = route;
