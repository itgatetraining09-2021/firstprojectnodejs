const mongoose = require('mongoose')
const user = require('./UserModel')

const schemaProvider = new mongoose.Schema({
    matricule:{
        type:String,
        required: true
    },
    company:{
        type:String,
        required: true
    },
    service:{
        type:String,
        required: true
    }
}, { timestamps:true})
module.exports = user.discriminator('Provider', schemaProvider)