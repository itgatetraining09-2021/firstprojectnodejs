const mongoose = require('mongoose')
const user = require('./UserModel')

const schemaCustomer = new mongoose.Schema(
	{
		address: {
			type: String,
			required: true,
		},
		city: {
			type: String,
			required: true,
		},
		id_order: [
			{
				//on ajoute tabulation si plusieurs//premiere etape relation many
				type: mongoose.Types.ObjectId,
				ref: "Order",
			},
		],
	},
	{ timestamps: true }
);
module.exports = user.discriminator('Customer', schemaCustomer)