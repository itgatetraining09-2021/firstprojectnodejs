const mongoose = require("mongoose");
const schemaCategory = new mongoose.Schema(
	{
		title: {
			type: String,
			required: true,
		},
		description: {
			type: String,
			required: true,
		},
	},
	{ timestamps: true }
); //pour ajouter createdat et updatedat des modeles
module.exports = mongoose.model("Category", schemaCategory);
