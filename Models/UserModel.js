const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const schemaUser = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
			minlength: 4,
		},
		email: {
			type: String,
			required: true,
			default: "",
			unique: true,
		},
		password: {
			type: String,
			required: true,
			default: "",
		},
		phone: {
			type: Number,
			required: true,
			default: "",
		},
		image: {
			type: String,
		},
	},
	{ timestamps: true }
); //pour ajouter createdat et updatedat des modeles
schemaUser.pre("save", function (next) {
	this.password = bcrypt.hashSync(this.password, 10); //pour avoir mdp chiffré
	next();
});
module.exports = mongoose.model("User", schemaUser);
