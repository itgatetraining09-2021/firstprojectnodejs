const mongoose = require("mongoose");
const schemaOrder = new mongoose.Schema(
	{
		date: {
			type: String,
			required: true,
		},
		price: {
			type: String,
			required: true,
		},
		id_product: [
			{
				//on ajoute tabulation si plusieurs//premiere etape relation many
				type: mongoose.Types.ObjectId,
				ref: "Product",
			},
		],
	},
	{ timestamps: true }
); //pour ajouter createdat et updatedat des modeles
module.exports = mongoose.model("Order", schemaOrder);
