const mongoose = require('mongoose')
const schemaProduct = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
		},
		price: {
			type: Number,
			required: true,
		},
		description: {
			type: String,
			required: true,
		},
		/* image: {
        type: String
    }, */
		id_category: {
			//premiere etape relation one
			type: mongoose.Types.ObjectId,
			ref: "Category",
		},
		id_provider: {
			//premiere etape relation one
			type: mongoose.Types.ObjectId,
			ref: "Provider",
		},
	},
	{ timestamps: true }
);//pour ajouter createdat et updatedat des modeles
module.exports = mongoose.model('Product', schemaProduct)