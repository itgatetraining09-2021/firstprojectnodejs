const express=require('express')
const cors=require('cors')
const bodyparser=require('body-parser')
const app=express()
const db=require('./config/database')
const userrouter=require('./routes/userrouter')
const Usercontrollers = require('./Controllers/UserController')

app.get("/getfile/:image", function (req, res) {
	//pour faire affichage image sur postman
	res.sendFile(__dirname + "/images/" + req.params.image);
});

const costomerrouter = require('./Routes/CustomerRouter')
const providerrouter = require('./Routes/ProviderRouter')
const categoryrouter = require('./Routes/CategoryRouter')
const productrouter = require('./Routes/ProductRouter')
const orderrouter = require('./Routes/OrderRouter')


app.use(cors())
app.use(bodyparser.json())
app.set('secretKey',"apibackend")//clé privé de chiffrement
app.use('/users',userrouter)
// app.get('/getfile/:image',function(req,res){//pour faire affichage image sur postman
//     res.sendFile(__dirname+'/images/'+req.params.image)
// }) 

app.use('/customers', costomerrouter)
app.use('/providers', providerrouter)
app.use('/categories', categoryrouter)
app.use('/products', productrouter)
app.use('/orders', orderrouter)


app.listen(3400,(err)=>{
    if(err) 
    console.log('erreur de connexion',err)
    else 
    console.log('server is running 3400')
})